﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Singleton;

public enum PanelType
{
    MainMenu,
    QuizView,
    FingerQuiz,
    ResultsPanel
}

public class PanelManager : Singleton<PanelManager> {

    #region Properties & Variables
    public Panel mainMenu;
    public Panel quizView;
    public Panel fingerQuiz;
    public Panel resultsPanel;

    private Panel currentPanel;
    private Dictionary<PanelType, Panel> panelsByType;

    #endregion Properties & Variables 


    #region Cached Components
    

    #endregion Cached Components


    #region MonoBehaviour Implementation
    void Start ()
    {
        SetupPanelDictionary();

        // Temp Debug Code
        SetPanel(PanelType.MainMenu);
    }
    #endregion MonoBehaviour Implementation


    #region Public Interface
    public void SetPanel(PanelType panelType)
    {
        if (currentPanel != null)
            currentPanel.TransitionOut();

        Panel targetPanel = panelsByType[panelType];
        targetPanel.TransitionIn();
        currentPanel = targetPanel;
    }
    #endregion Public Interface

    
    #region Private Helpers
    private void SetupPanelDictionary()
    {
        panelsByType = new Dictionary<PanelType, Panel>()
        {
            { PanelType.MainMenu, mainMenu },
            { PanelType.QuizView, quizView },
            { PanelType.ResultsPanel, resultsPanel }
        };
    }
    #endregion Private Helpers
}
