﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizView : Panel {

    #region Cached Components        
    public HornSelector hornSelector;
    public QuizTypeSelector quizTypeSelector;
	public MainMenu mainMenu;
	public ResultsPanel resultsPanel;
    [Space]
    public GameObject backButton;
    public NoteView noteView;
    public ProgressBar progressBar;
    public QuestionResultDisplay resultsDisplay;
    public Text questionText;
    [Space]
    // Input fields for each quiz type
    public FingerInput fingerInput;
    public NoteInput noteInput;
    public PitchInput pitchInput;
    [Space]
	public Button nextButton;
	public Button quitButton;
    public float hornDelayTime = .25f;
    public float quizTypeDelayTime = .25f;
    #endregion Cached Components


    #region Properties & Variables
    public Horn activeHorn;
    public QuizType currentQuizType;
    public Quiz currentQuiz;

    private QuizCard currentQuestion;

    private const string STATE_HORN_SELECT = "horn selection";
    private const string STATE_TYPE_SELECT = "type selection";
    private const string STATE_QUIZ_ACTIVE = "active quiz";
    private string currentState = STATE_TYPE_SELECT;

    private const string QUESTION_NOTE = "What is the note below?";
    private const string QUESTION_FINGERING = "What is the correct fingering for the note below?";
    private const string QUESTION_PITCH = "What is the pitch tendency of the note below?";

	private bool quit = false;
    #endregion Properties & Variables


    #region MonoBehaviour Implementation
    
    #endregion MonoBehaviour Implementation


    #region Public Interface
    private void ResetQuiz()
    {
		if (quit) 
		{
			quit = false;
			mainMenu.transitionComplete -= ResetQuiz;
		}
		
        currentState = STATE_TYPE_SELECT;
        quizTypeSelector.gameObject.SetActive(false);
        hornSelector.gameObject.SetActive(false);
        noteView.ResetView();
        nextButton.gameObject.SetActive(false);
		quitButton.gameObject.SetActive (false);

		progressBar.fillBar.fillAmount = 0f;
		progressBar.gameObject.SetActive(false);
		questionText.gameObject.SetActive(false);

		if (currentQuizType == QuizType.Note && noteInput.gameObject.activeInHierarchy) 
		{
			noteInput.questionAnswered -= OnNoteAnswered;
			noteInput.Close ();
		}
		if (currentQuizType == QuizType.Pitch) 
		{
			pitchInput.pitchSelected -= OnPitchAnswered;
			pitchInput.Close ();
		}
		if (currentQuizType == QuizType.Fingering) 
		{
			fingerInput.questionAnswered -= OnFingeringAnswered;
			fingerInput.Close ();
		}
    }

    public void SetHorn(Horn selection)
    {
        activeHorn = selection;
        currentState = STATE_QUIZ_ACTIVE;

        hornSelector.Close();
        backButton.SetActive(false);

        StartCoroutine(DelayQuizInit(hornDelayTime));        
    }

    public void SetQuizType(QuizType type)
    {
        currentQuizType = type;

        if (type == QuizType.Note)
        {
            currentState = STATE_QUIZ_ACTIVE;
            quizTypeSelector.Close();

            backButton.SetActive(false);

            StartCoroutine(DelayQuizInit(quizTypeDelayTime));
        }
        else
        {
            currentState = STATE_HORN_SELECT;
            hornSelector.gameObject.SetActive(true);
            quizTypeSelector.Close();
        }
    }

    public void GoBack()
    {
        if (currentState == STATE_TYPE_SELECT)
        {
            quizTypeSelector.onClose += OnTypeSelectorClosed;
            quizTypeSelector.Close();
            
        }
        else if (currentState == STATE_HORN_SELECT)
        {
            hornSelector.onClose += OnHornSelectorClosed;
            hornSelector.Close();            
        }
    }

    // Called when the user presses the next button to continue on to the next question
    public void NextQuestion()
    {
        if (currentQuiz.RemainingQuestions() == 0)
        {
			resultsPanel.percentageText.text = "";
            QuizManager.instance.SetResultQuiz(currentQuiz);
            ResetQuiz();
            PanelManager.instance.SetPanel(PanelType.ResultsPanel);
        }
        else
        {
            nextButton.gameObject.SetActive(false);

            currentQuestion = currentQuiz.GetNextQuestion();

            if (currentQuizType == QuizType.Note)
            {
                noteView.DisplayNote(currentQuestion);
                noteInput.SetOptions(currentQuestion);
            }

            if (currentQuizType == QuizType.Pitch)
            {
                Debug.LogWarning("Correct Answer: " + currentQuestion.GetPitchTendencyForHorn(activeHorn));
                noteView.DisplayNote(currentQuestion);
                pitchInput.ResetInput();
            }

            if (currentQuizType == QuizType.Fingering)
            {
                Fingering fff = currentQuestion.GetFingeringForHorn(activeHorn);
                Debug.LogWarning(fff.first + "-" + fff.second + "-" + fff.third);
                fingerInput.ResetValves();
                noteView.DisplayNote(currentQuestion);
            }
        }
    }

	public void QuitQuizInMiddle()
	{
		mainMenu.transitionComplete += ResetQuiz;
		quit = true;
		PanelManager.instance.SetPanel (PanelType.MainMenu);
	}
    #endregion Public Interface


    #region Animation Events
    public override void OnTransitionIn()
    {
        backButton.SetActive(true);
        quizTypeSelector.gameObject.SetActive(true);
    }
    #endregion Animation Events


    #region Event Handlers
    private void OnHornSelectorClosed()
    {
        hornSelector.onClose -= OnHornSelectorClosed;
        quizTypeSelector.gameObject.SetActive(true);
        currentState = STATE_TYPE_SELECT;
    }

    private void OnTypeSelectorClosed()
    {
        quizTypeSelector.onClose -= OnTypeSelectorClosed;
		ResetQuiz();
        PanelManager.instance.SetPanel(PanelType.MainMenu);
    }

    // Called when the user selects an aswer in a Note quiz
    private void OnNoteAnswered(Note answer)
    {
        if (answer == currentQuestion.note)
        {
            currentQuiz.SetCurrentAnswer(true);       
			noteInput.DisplayResult(true);
        }
        else
        {
            currentQuiz.SetCurrentAnswer(false);
            noteInput.DisplayResult(false);
        }

        progressBar.UpdateProgress(currentQuiz);
        noteInput.ShowCorrectAnswer();
        ShowNextButton();
    }

    // Called by the FingerInput when the user taps the submit button
    private void OnFingeringAnswered(Fingering answer)
    {
        Debug.Log(answer.ToString());
        Fingering correctAnswer = currentQuestion.GetFingeringForHorn(activeHorn);
        if (answer.SameAs(correctAnswer))
        {
            Debug.Log("RIGHT");
            fingerInput.ShowAnswer(correctAnswer, true);
            currentQuiz.SetCurrentAnswer(true);
            
        }
        else
        {
            Debug.Log("WRONG");
            fingerInput.ShowAnswer(correctAnswer, false);
            currentQuiz.SetCurrentAnswer(false);
            
        }

        progressBar.UpdateProgress(currentQuiz);
        ShowNextButton();
    }

    private void OnPitchAnswered(PitchTendency answer)
    {
        PitchTendency correctAnswer = currentQuestion.GetPitchTendencyForHorn(activeHorn);
        if (answer == correctAnswer)
        {
            currentQuiz.SetCurrentAnswer(true);
			pitchInput.DisplayResult(true, correctAnswer);
        }
        else
        {
			pitchInput.DisplayResult(false, correctAnswer);
            currentQuiz.SetCurrentAnswer(false);
        }

        progressBar.UpdateProgress(currentQuiz);
        ShowNextButton();
    }
    #endregion Event Handlers


    #region Private Helpers
    private void CreateQuizAndInitializeView()
    {
        currentQuiz = QuizManager.instance.GenerateQuiz();
		progressBar.ResetProgress ();


        if (currentQuizType == QuizType.Note)
        {
            noteInput.gameObject.SetActive(true);
            noteInput.questionAnswered += OnNoteAnswered;
			SetQuestionText (QUESTION_NOTE);
        }

        if (currentQuizType == QuizType.Fingering)
        {
            fingerInput.gameObject.SetActive(true);
            fingerInput.questionAnswered += OnFingeringAnswered;
			SetQuestionText (QUESTION_FINGERING);
        }

        if (currentQuizType == QuizType.Pitch)
        {
            pitchInput.gameObject.SetActive(true);
            pitchInput.pitchSelected += OnPitchAnswered;
			SetQuestionText (QUESTION_PITCH);
        }

		quitButton.gameObject.SetActive(true);
		questionText.gameObject.SetActive(true);
        progressBar.gameObject.SetActive(true);

        NextQuestion();
    }

    // Called when the user answers the current question, sets the button active to move to the next question
    private void ShowNextButton()
    {
        // TODO: Handle case where this is the final question
        nextButton.gameObject.SetActive(true);
    }

    private IEnumerator DelayQuizInit(float time)
    {
        float timer = 0f;
        while (timer < time)
        {
            timer += Time.deltaTime;
            yield return null;
        }

        CreateQuizAndInitializeView();
    }

	private void SetQuestionText(string text)
	{
		questionText.text = text;
	}
    #endregion Private Helpers

}
