﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuizTypeSelector : MonoBehaviour {

    public Animator animator;
    public QuizView quizView;

    public event System.Action onClose;

    private void OnEnable()
    {
        animator.Play("open");
    }

    public void DisableOnClose()
    {
        if (onClose != null)
            onClose();

        gameObject.SetActive(false);
    }

    public void Close()
    {
        animator.Play("close");
    }

    public void SelectQuizType(QuizType type)
    {
        quizView.SetQuizType(type);
    }


    #region UI Events
    public void OnNoteSelected()
    {
        SelectQuizType(QuizType.Note);
    }

    public void OnFingeringSelected()
    {
        SelectQuizType(QuizType.Fingering);
    }

    public void OnPitchSelected()
    {
        SelectQuizType(QuizType.Pitch);
    }
    #endregion UI Events
}
