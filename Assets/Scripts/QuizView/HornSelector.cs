﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HornSelector : MonoBehaviour {

    public Animator animator;
    public QuizView quizView;
    public event System.Action onClose;

    void OnEnable()
    {
        animator.Play("open");    
    }

    public void SetHorn(Horn selection)
    {
        quizView.SetHorn(selection);

        animator.Play("close");
    }

    public void Close()
    {
        animator.Play("close");
    }

    #region UI Events
    public void SetFHorn()
    {
        SetHorn(Horn.F);
    }

    public void SetBFlatHorn()
    {
        SetHorn(Horn.Bb);
    }

    public void SetDoubleHorn()
    {
        SetHorn(Horn.Double);
    }
    #endregion UI Events

    // Called as an animation Event on closing
    public void DiableAfterClosing()
    {
        if (onClose != null)
            onClose();

        gameObject.SetActive(false);
    }
}
