﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultsPanel : Panel {
    
    public Text percentageText;
    public Button menuButton;
    [Space]
    public float tweenTime = 1f;

    private Quiz quiz;

    public void OnTransitionComplete()
    {
        quiz = QuizManager.instance.resultsQuiz;
        TweenPercentage();
    }

    public void ReturnToMenu()
    {
		iTween.Stop ();
		percentageText.text = "";
		menuButton.gameObject.SetActive (false);
        PanelManager.instance.SetPanel(PanelType.MainMenu);
    }

    public void TweenPercentage()
    {
        int percentage = (int)(quiz.GetPercentageCorrect() * 100);

        Debug.LogWarning("Percentage Correct: " + quiz.GetPercentageCorrect());
        Debug.LogWarning("Int: " + percentage);

        iTween.ValueTo(gameObject, iTween.Hash(
            "from", 0,
            "to", percentage,
            "time", tweenTime,
            "onupdatetarget", gameObject,
            "onupdate", "OnPercentageTween",
			"oncomplete", "OnTweenComplete",
            "easetype", iTween.EaseType.easeOutQuad
         ));
    }

    private void OnPercentageTween(int newValue)
    {
        percentageText.text = FormatPercentageString(newValue);
    }

    private string FormatPercentageString(int num)
    {
        return "" + num + "%";
    }

	private void OnTweenComplete()
	{
		menuButton.gameObject.SetActive (true);
	}
}
