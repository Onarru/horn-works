﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Panel : MonoBehaviour {

    public Animator animator;

    private const string ANIM_TRANSITION_IN = "transition_in";
    private const string ANIM_TRANSITION_OUT = "transition_out";

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void TransitionIn()
    {
        animator.Play(ANIM_TRANSITION_IN);
    }

    public void TransitionOut()
    {
        animator.Play(ANIM_TRANSITION_OUT);
    }

    public virtual void OnTransitionIn() { }
    public virtual void OnTransitionOut() { }
}
