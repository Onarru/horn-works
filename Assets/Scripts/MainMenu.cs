﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : Panel {
    
	public event System.Action transitionComplete;

    public void StartQuiz()
    {
        PanelManager.instance.SetPanel(PanelType.QuizView);
    }

	public void OnTransitionIn()
	{
		if (transitionComplete != null)
			transitionComplete ();
	}
}
