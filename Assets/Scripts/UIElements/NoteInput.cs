﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoteInput : MonoBehaviour {

    public Animator animator;

    public AnswerButton ButtonOne;
    public AnswerButton ButtonTwo;
    public AnswerButton ButtonThree;
    public AnswerButton ButtonFour;

    public Text resultText;

    public event System.Action<Note> questionAnswered;

    // Info for the current question
    private QuizCard currentCard;
    private int correctButtonIndex;
    private int lastUserAnswer;

	private const string STATE_NORMAL = "normal";
	private const string STATE_CORRECT = "correct";
	private const string STATE_INCORRECT = "incorrect";
	private string currentState;

    private void Start()
    {
		currentState = STATE_NORMAL;
        ButtonOne.onAnswer += QuestionAnswered;
        ButtonTwo.onAnswer += QuestionAnswered;
        ButtonThree.onAnswer += QuestionAnswered;
        ButtonFour.onAnswer += QuestionAnswered;
    }

    #region Public Interface
    public void SetOptions(QuizCard card)
    {
		if (currentState == STATE_CORRECT)
			animator.Play ("hide_correct");
		if (currentState == STATE_INCORRECT)
			animator.Play ("hide_incorrect");
		else
        	animator.Play("hidden");

        currentCard = card;
        Debug.LogWarning("Correct Answer: " + card.note);

        List<Note> randomNotes = NoteUtils.GetRandomNotes(4, card.note);

        int randomButton = Random.Range(0, 4);
        for (int i = 0; i < randomNotes.Count; i++)
        {
            AnswerButton nextButton = GetButtonFromNumber(i);
            if (i == randomButton)
            {
                Debug.LogWarning("Setting button " + (i + 1) + " to correct answer");
                nextButton.SetAnswer(card.note);
            }
            else
                nextButton.SetAnswer(randomNotes[i]);            
        }

        EnableButtons();
    }

    public void DisplayResult(bool correct)
    {
		resultText.text = NoteUtils.StringFromNote (currentCard.note);
		if (!correct) 
		{
			currentState = STATE_INCORRECT;
			animator.Play ("incorrect");
		} 
		else 
		{
			currentState = STATE_CORRECT;
			animator.Play ("correct");
		}
    }

    public void ShowCorrectAnswer()
    {
        // Disable all buttons to prevent further input on same question
        DisableButtons();
        
        if (lastUserAnswer != correctButtonIndex)
        {
            GetButtonFromNumber(lastUserAnswer).DisplayInCorrect();
        }
    }

    public void Close()
    {
		
        animator.Play("hidden");
		currentState = STATE_NORMAL;
        gameObject.SetActive(false);
    }

    #endregion Public Interface


    #region Private Helpers
    private AnswerButton GetButtonFromNumber(int num)
    {
        if (num == 0)
            return ButtonOne;
        if (num == 1)
            return ButtonTwo;
        if (num == 2)
            return ButtonThree;
        if (num == 3)
            return ButtonFour;
        else
        {
            Debug.LogError("Got a number outside 0-3 range: " + num);
            return null;
        }
    }

    private AnswerButton GetButtonFromNote(Note note)
    {
        if (note == ButtonOne.note)
            return ButtonOne;
        if (note == ButtonTwo.note)
            return ButtonTwo;
        if (note == ButtonThree.note)
            return ButtonThree;
        if (note == ButtonFour.note)
            return ButtonFour;
        else
        {
            Debug.LogError("Got a note that wasn't on a butt: " + note);
            return null;
        }
    }
    
    private void QuestionAnswered(Note answer)
    {
        if (questionAnswered != null)
            questionAnswered(answer);
    }

    private void DisableButtons()
    {
        ButtonOne.button.enabled = false;
        ButtonTwo.button.enabled = false;
        ButtonThree.button.enabled = false;
        ButtonFour.button.enabled = false;
    }

    private void EnableButtons()
    {
        ButtonOne.button.enabled = true;
        ButtonTwo.button.enabled = true;
        ButtonThree.button.enabled = true;
        ButtonFour.button.enabled = true;
    }
    #endregion Private Helpers
}
