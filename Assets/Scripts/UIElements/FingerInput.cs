﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FingerInput : MonoBehaviour {

    public Animator animator;
    [Space]
    public Animator circleOne;
    public Animator circleTwo;
    public Animator circleThree;
    [Space]
    public Animator resultOne;
    public Animator resultTwo;
    public Animator resultThree;
    [Space]
    public Button submitButton;
    
    [HideInInspector]
    public bool[] selectedValves = new bool[] { false, false, false };
    public bool[] resultValves = new bool[] { false, false, false };
    public event System.Action<Fingering> questionAnswered;
    private Fingering correctAnswer;

    private const string STATE_NORMAL = "normal";
    private const string STATE_CORRECT = "correct";
    private const string STATE_INCORRECT = "incorrect";
    private string currentState = STATE_NORMAL;

	public void OnEnable()
	{
		if (!submitButton.gameObject.activeInHierarchy)
			submitButton.gameObject.SetActive (true);
	}

    public void OnSelected(int valveNumber)
    {
        if (!selectedValves[valveNumber])
            GetValveFromNumber(valveNumber).Play("select");
        else
            GetValveFromNumber(valveNumber).Play("unselect");

        selectedValves[valveNumber] = !selectedValves[valveNumber];        
    }

    public void ShowAnswer(Fingering answer, bool correct)
    {
        correctAnswer = answer;
        if (correct)
        {
            currentState = STATE_CORRECT;
            animator.Play("correct");
        }
        else
        {
            currentState = STATE_INCORRECT;
            animator.Play("incorrect");
        }
    }

    public void ResetValves()
    {
        if (selectedValves[0])
            circleOne.Play("unselect");
        if (selectedValves[1])
            circleTwo.Play("unselect");
        if (selectedValves[2])
            circleThree.Play("unselect");

        if (currentState == STATE_CORRECT)
        {
            animator.Play("hide_correct");
        }
        else if (currentState == STATE_INCORRECT)
        {
            if (resultValves[0])
                resultOne.Play("unselect");
            if (resultValves[1])
                resultTwo.Play("unselect");
            if (resultValves[2])
                resultThree.Play("unselect");

            resultValves = new bool[] { false, false, false };
            animator.Play("hide_incorrect");
        }
        
        currentState = STATE_NORMAL;
        selectedValves = new bool[] { false, false, false };        
    }

	public void Close()
	{
		currentState = STATE_NORMAL;
		animator.Play ("normal");
		gameObject.SetActive (false);
	}


    #region UI Events
    public void OnSubmit()
    {
        if (questionAnswered != null)
        {
            submitButton.gameObject.SetActive(false);
            Fingering answer = new Fingering(selectedValves[0], selectedValves[1], selectedValves[2]);
            questionAnswered(answer);
        }
    }
    #endregion UI Events


    #region Animation Event Handlers
    // Called at the end of the incorrect answer animation to display the correct answer
    public void OnDisplayAnswer()
    {
        if (correctAnswer.first)
        {
            resultOne.Play("select");
            resultValves[0] = true;
        }
        if (correctAnswer.second)
        {
            resultTwo.Play("select");
            resultValves[1] = true;
        }
        if (correctAnswer.third)
        {
            resultThree.Play("select");
            resultValves[2] = true;
        }
    }

	public void OnAnswerHidden()
	{
		submitButton.gameObject.SetActive (true);
	}
    #endregion Animation Event Handlers


    #region Private Helpers
    private Animator GetValveFromNumber(int num)
    {
        if (num == 0)
            return circleOne;
        if (num == 1)
            return circleTwo;
        if (num == 2)
            return circleThree;
        else
        {
            Debug.LogError("Bad Valve number " + num);
            return null;
        }
    }
    #endregion Private Helpers

}
