﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoteView : MonoBehaviour {

    public Animator animator;
    public Image noteImage;

    private Note currentNote;    
    private QuizCard nextCard;

    
    public void DisplayNote(QuizCard card)
    {
        nextCard = card;
        if (noteImage.sprite == null)
        {
            noteImage.sprite = Resources.Load<Sprite>("NoteSprites/" + nextCard.ImageFilename());
            animator.Play("showNote");
        }    
        else
        {
            animator.Play("hideNote");
        }
    }

    public void ResetView()
    {
        noteImage.sprite = null;
    }

    #region Animation Events
    private void ImageHidden()
    {
        noteImage.sprite = Resources.Load<Sprite>("NoteSprites/" + nextCard.ImageFilename());
        animator.Play("showNote");
    }
    #endregion Animation Events
}