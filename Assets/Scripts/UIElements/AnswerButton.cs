﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnswerButton : MonoBehaviour {
    
    [Space]
    public Button button;
    public Text text;
    
    public Note note;

    public event System.Action<Note> onAnswer;
    

    public void SetAnswer(Note note)
    {
        ResetCorrect();
        text.text = NoteUtils.StringFromNote(note);
        this.note = note;
    }

    public void OnAnswer()
    {
        if (onAnswer != null)
            onAnswer(note);
    }

    // Sets the button to show the user that it was/is the correct answer
    public void DisplayCorrect()
    {
        
    }

    // Sets the button to show the user that it was not the correct answer
    public void DisplayInCorrect()
    {
        
    }

    // Returns the button back to normal
    public void ResetCorrect()
    {
       
    }
}
