﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionResultDisplay : MonoBehaviour {

    public Text resultText;
    public Image resultImage;
    [Space]
    public Sprite correctSprite;
    public Sprite inCorrectSprite;
    [Space]
    public Color correctColor;
    public Color inCorrectColor;

    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void DisplayResult(bool correct)
    {
        if (correct)
        {
            resultImage.sprite = correctSprite;
            resultImage.color = correctColor;
        }
        else
        {
            resultImage.sprite = inCorrectSprite;
            resultImage.color = inCorrectColor;
        }
        animator.Play("show_result");
    }
}
