﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PitchInput : MonoBehaviour {

    public Animator animator;
    public event System.Action<PitchTendency> pitchSelected;
    [Space]
    public Button sharpButton;
    public Button flatButton;
    public Button okButton;
    [Space]
    public Text answerText;

    private const string STATE_CORRECT = "correct";
    private const string STATE_INCORRECT = "incorrect";
    private const string STATE_NORMAL = "normal";

    private string currentState;

    #region Public Interface
    public void ResetInput()
    {
		if (currentState == STATE_INCORRECT) 
		{
			animator.Play("hide_incorrect");
		} 
		else if (currentState == STATE_CORRECT) 
		{
			animator.Play("hide_correct");
		}
		else
			EnableButtons();
		
        currentState = STATE_NORMAL;
    }

	public void DisplayResult(bool correct, PitchTendency correctPitch)
    {	
        string answerString = "";
        if (correctPitch == PitchTendency.Flat)
            answerString = "Flat";
        if (correctPitch == PitchTendency.Sharp)
            answerString = "Sharp";
        if (correctPitch == PitchTendency.OK)
            answerString = "OK (In Tune)";

		answerText.text = answerString;

		if (correct) 
		{
			currentState = STATE_CORRECT;
			animator.Play ("correct");
		} 
		else 
		{
			currentState = STATE_INCORRECT;
			animator.Play ("incorrect");
		}
    }

	public void Close()
	{
		currentState = STATE_NORMAL;
		animator.Play ("normal");
		gameObject.SetActive (false);
	}
    #endregion Public interface


    #region UI Events
    public void OnPitchSelected(PitchTendency answer)
    {
        if (pitchSelected != null)
        {
            DisableButtons();
            pitchSelected(answer);
        }
    }

    public void SharpSelected()
    {
        OnPitchSelected(PitchTendency.Sharp);
    }

    public void OkSelected()
    {
        OnPitchSelected(PitchTendency.OK);
    }

    public void FlatSelected()
    {
        OnPitchSelected(PitchTendency.Flat);
    }
    #endregion UI Events


    #region Animation Events
    public void OnAnswerHidden()
    {
        EnableButtons();
    }
    #endregion Animation Events


    #region Private Helpers
    private void DisableButtons()
    {
        flatButton.enabled = false;
        sharpButton.enabled = false;
        okButton.enabled = false;
    }

    private void EnableButtons()
    {
        flatButton.enabled = true;
        sharpButton.enabled = true;
        okButton.enabled = true;
    }
    #endregion Private Helpers
}
