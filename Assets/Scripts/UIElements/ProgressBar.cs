﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour {

    public Image fillBar;
    public float fillTime = 1;

	void OnEnable()
	{
		fillBar.fillAmount = 0f;
	}

	public void UpdateProgress(Quiz quiz)
    {
        float newFill = quiz.GetPercentageDone();
        TweenFill(newFill);
    }

    public void ResetProgress()
    {
		iTween.Stop();
		fillBar.fillAmount = 0f;
    }

    private void TweenFill(float newValue)
    {
        iTween.ValueTo(gameObject, iTween.Hash(
             "from", fillBar.fillAmount,
             "to", newValue,
             "time", fillTime,
             "onupdatetarget", gameObject,
             "onupdate", "OnFillTween",
             "easetype", iTween.EaseType.easeOutQuad
         ));
    }

    private void OnFillTween(float amount)
    {
        fillBar.fillAmount = amount;
    }
}
