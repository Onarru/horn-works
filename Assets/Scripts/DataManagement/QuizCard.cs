﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Note
{
    C,
    C_Sharp,
    D_Flat,
    D,
    D_Sharp,
    E_Flat,
    E,
    F,
    F_Sharp,
    G_Flat,
    G,
    G_Sharp,
    A_Flat,
    A,
    A_Sharp,
    B_Flat,
    B
}

public enum PitchTendency
{
    OK,
    Sharp,
    Flat
}

public enum Horn
{
    F,
    Bb,
    Double
}

[System.Serializable]
public class QuizCard {

    public int id;
    
    public Note note;
    public int octave = 4;
    [Space]

    [Header("Fingerings")]
    public Fingering f;
    public Fingering bb;
    public Fingering dbl;

    [Header("Pitch Tendencies")]
    public PitchTendency fPitchTendency;
    public PitchTendency bbPitchTendency;
    public PitchTendency doublePitchTendency;

    public string ImageFilename()
    {
        return note + "_" + octave;
    }

    public Fingering GetFingeringForHorn(Horn horn)
    {
        if (horn == Horn.F)
            return f;
        if (horn == Horn.Bb)
            return bb;
        else
            return dbl;        
    }

    public PitchTendency GetPitchTendencyForHorn(Horn horn)
    {
        if (horn == Horn.F)
            return fPitchTendency;
        if (horn == Horn.Bb)
            return bbPitchTendency;
        else
            return doublePitchTendency;
    }
}

[System.Serializable]
public class Fingering
{
    public bool first = true;
    public bool second = false;
    public bool third = true;

    public Fingering(bool first, bool second, bool third)
    {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public bool[] GetFingers()
    {
        return new bool[]
        {
            first, second, third
        };
    }
    
    public override string ToString()
    {
        return first + "-" + second + "-" + third;
    }

    public bool SameAs(Fingering other)
    {
        return (
            other.first == this.first &&
            other.second == this.second &&
            other.third == this.third
        );
    }
}


public static class NoteUtils
{
    public static Dictionary<Note, string> noteDict = new Dictionary<Note, string>
    {
        { Note.C, "C" },
        { Note.C_Sharp,"C#" },
        { Note.D_Flat, "D♭" },
        { Note.D, "D" },
        { Note.D_Sharp, "D#" },
        { Note.E_Flat, "E♭" },
        { Note.E, "E" },
        { Note.F, "F" },
        { Note.F_Sharp, "F#" },
        { Note.G_Flat, "G♭" },
        { Note.G, "G" },
        { Note.G_Sharp, "G#" },
        { Note.A_Flat, "A♭" },
        { Note.A, "A" },
        { Note.A_Sharp, "A#" },
        { Note.B_Flat, "B♭" },
        { Note.B, "B" }
    };

    public static string StringFromNote(Note note)
    {
        return noteDict[note];
    }

    public static List<Note> GetRandomNotes(int count, Note noteToAvoid)
    {
        List<int> pickedNotes = new List<int>();
        int avoidIndex = new List<Note>(noteDict.Keys).IndexOf(noteToAvoid);

        for (int num = 0; num < count; num++)
        {
            int randomIndex = Random.Range(0, noteDict.Keys.Count);
            while (pickedNotes.Contains(randomIndex) || randomIndex == avoidIndex)
                randomIndex = Random.Range(0, noteDict.Keys.Count);

            pickedNotes.Add(randomIndex);
        }

        List<Note> notes = new List<Note>();
        foreach (int noteIndex in pickedNotes)
        {
            notes.Add(GetNoteFromIndex(noteIndex));
        }

        return notes;
    }

    private static Note GetNoteFromIndex(int index)
    {
        int i = 0;
        foreach (var kvp in noteDict)
        {
            if (i == index)
            {
                return kvp.Key;
            }
            i++;
        }

        Debug.LogError("INDEX IS " + index);
        Debug.LogError("Unable to get random note. Returning C");
        return Note.C;
    }
}