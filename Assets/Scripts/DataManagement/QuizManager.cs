﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Singleton;

public enum QuizType
{
    Note,
    Fingering,
    Pitch
}

public class QuizManager : Singleton<QuizManager> {

    public const int quizSize = 20;
    public List<QuizCard> cardPool;

    private int[] debugCounts = new int[45];
    private int testCount = 1000;

    public Quiz resultsQuiz;

    public Quiz GenerateQuiz(int size=quizSize)
    {
        List<QuizCard> cards = RandomlySelectCards(20);

        return new Quiz(cards);
    }

    public void SetResultQuiz(Quiz quiz)
    {
        this.resultsQuiz = quiz;
    }

    // This is a bad algorithm but works well enough for out purposes
    private List<QuizCard> RandomlySelectCards(int count)
    {
        List<QuizCard> cards = new List<QuizCard>(cardPool);
		Shuffle (cards);
        
        for (int i = cardPool.Count; i > count; i--)
        {
            int r = Random.Range(0, cards.Count);
            cards.RemoveAt(r);
            Debug.Log("Removing card at " + r);
        }

        return cards;
    }

	private void Shuffle(List<QuizCard> cards)
	{
		for (int i = 0; i < cards.Count; i++) 
		{
			int r = Random.Range (i, cards.Count);
			QuizCard temp = cards[i];
			cards [i] = cards [r];
			cards [r] = temp;
		}
	}
}


