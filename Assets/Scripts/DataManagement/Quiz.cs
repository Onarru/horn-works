﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quiz {

    public List<QuizCard> questions;
    public Dictionary<int, bool> results;

    public int answered = 0;

    private int currentIndex = 0;

    public Quiz(List<QuizCard> cards)
    {
        questions = cards;
        results = new Dictionary<int, bool>();        
    }

    public QuizCard GetNextQuestion()
    {
        Debug.Log("Current index is " + currentIndex);
        QuizCard nextCard = questions[currentIndex];
        currentIndex++;
        return nextCard;
    }

    public void SetCurrentAnswer(bool correct)
    {
        results.Add(currentIndex - 1, correct);
        answered++;
    }

    public float GetPercentageCorrect()
    {
        int correctCount = 0;
        for (int i = 0; i < questions.Count; i++)
        {
            if (results[i])
                correctCount++;        
        }

        return (float)correctCount / (float)questions.Count;    
    }

    public float GetPercentageDone()
    {
        return (float)answered / (float)questions.Count;
    }

    public int RemainingQuestions()
    {
        Debug.LogWarning(questions.Count - answered);
        return questions.Count - answered;
    }
}
